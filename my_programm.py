from pyfeeder.models.feed_record import FeedRecord
from .dagger import Link, Dagger, Node


class FileReader:
    pass

class Error:
    pass

class ImageDownloader:
    pass

class Printer:
    pass

class Unindexed:
    pass

if __name__ == '__main__':
    dag = Dagger(nodes=[
        Node("file_reader", FileReader, into=[
            Link("image_download", FeedRecord),
            Link("unindexed", Error)
        ]),
        Node("image_download", ImageDownloader, pool=100, into=[
            Link("printer", FeedRecord),
            Link("unindexed", Error)
        ]),
        Node("printer", Printer, [], pool=100),
        Node("unindexed", Unindexed, []),
    ])

    results = dag.run("./test.json")
    print(results)
