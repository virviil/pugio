import asyncio
from typing import Dict, List, Set
from .node import Node


class DagError(Exception):
    pass


class ExecutionGraph:
    def __init__(
        self,
        execution_nodes: List[Node],
        execution_queues: List[asyncio.Queue],
        execution_queue_index: Dict[str, asyncio.Queue],
    ) -> None:
        self.execution_nodes = execution_nodes
        self.execution_queues = execution_queues
        self.execution_queue_index = execution_queue_index


def generate_edges(nodes: List[Node]) -> List[List[str]]:
    edges: List[List[str]] = []
    for node in nodes:
        for link_name in node.links_names:
            new_edge = [node.name, link_name]
            if new_edge in edges:
                raise DagError(f"Link {new_edge} is found twice, DAG is malformed")
            edges.append([node.name, link_name])
    return edges


def generate_node_index(nodes: List[Node]) -> Dict[str, Node]:
    node_index = {}
    for node in nodes:
        node_index[node.name] = node
    return node_index


def validate_isolated_nodes(edges: List[List[str]], nodes: List[str]) -> None:
    current_nodes = []
    for edge in edges:
        if edge[0] not in current_nodes:
            current_nodes.append(edge[0])
        if edge[1] not in current_nodes:
            current_nodes.append(edge[1])
    if set(nodes) != set(current_nodes):
        raise DagError(f"DAG is malformed, some nodes are not connected by the links")


def find_root(edges: List[List[str]], nodes: List[Node]) -> Node:
    roots = []
    for node in nodes:
        if node not in [edge[1] for edge in edges]:
            roots.append(node)

    if len(roots) != 1:
        raise DagError(f"DAG is malformed, more than one root found")

    return roots[0]


def build_execution_graph(nodes: List[Node]):
    node_index = generate_node_index(nodes)
    edges = generate_edges(nodes)
    validate_isolated_nodes(edges, list(node_index.keys()))
    root = find_root(edges, nodes)

    execution_nodes = []
    initial_queue: asyncio.Queue = asyncio.Queue()

    # Here we are starting to build an index. If queue is already presented,
    # we are going to use it. If not, we are going to create it.
    execution_queue_index = {root.name: initial_queue}

    # Here is a list of queues to join.
    # They can duplicae, so we can join one queue several times
    execution_queues = []
    nodes_queue = [root]

    unchecked_nodes = set([name for name in node_index.keys()])

    # This cycle will probably hangs if there are cycles in the graph
    while len(nodes_queue) > 0:
        current_node = nodes_queue.pop(0)

        # Here producing workers only if the nodes
        # are not checked - so workers are not created for them
        if current_node.name in unchecked_nodes:
            # Add the node to execution plan
            execution_nodes.append(current_node)

            # Add all the links to the next elements for review
            for link in current_node.links:
                # Add nodes to nodes queue
                nodes_queue.append(node_index[link.linked_node_name])

            # GET input queue for the current node
            current_node_input_queue = execution_queue_index[current_node.name]

            # If its not defined - we are going create new one
            if current_node_input_queue is None:
                current_node_input_queue = asyncio.Queue()
                execution_queue_index[current_node.name] = current_node_input_queue

            # Appending OR existing OR newly created
            execution_queues.append(current_node_input_queue)

            # Checking the node
            unchecked_nodes.remove(current_node.name)

        # Here node is checked - nothing to do with workers, but need to join on it's input queue
        else:
            # Appending queue - it must exists for already checked node
            execution_queues.append(execution_queue_index[current_node.name])

    return (execution_nodes, execution_queues, execution_queue_index, root)
