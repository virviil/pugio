import logging
import asyncio
from typing import Any, Dict


logger = logging.getLogger("pugio")


class Executor:
    def __init__(*args, **kwargs):
        raise NotImplementedError

    async def create_task_coroutine(
        self, input_queue: asyncio.Queue, output_queues: Dict[type, asyncio.Queue]
    ) -> None:
        try:
            while True:
                input = await input_queue.get()
                try:
                    result = await self.run(input)
                    results_queue = output_queues[type(result)]

                    if results_queue is not None:
                        await results_queue.put(result)

                    # If results queue is not found - drop the results and continue
                    # These results must be separated from the None results

                    if result is not None:
                        logger.warn(
                            f"Result {result} is dropped because no suitable out queue is found"
                        )

                finally:
                    input_queue.task_done()
        except asyncio.CancelledError:
            return None

    async def run(self, data: Any) -> Any:
        raise NotImplementedError
