import asyncio

from pugio.metrics import Metrics
from typing import Any, List
from .node import Node

from .graph import build_execution_graph


class Pugio:
    def __init__(self, nodes: List[Node] = []) -> None:

        # Queues part:
        self.__metrics_queue = asyncio.Queue()

        self.__nodes = nodes

        # Validation part

        # Validate all links
        for node in self.__nodes:
            if not node.ensure_links_valid(self.__nodes_names):
                raise ValueError("Node '{}' has invalid links".format(node.name))

        (
            execution_nodes,
            execution_queues,
            execution_queue_index,
            root,
        ) = build_execution_graph(self.__nodes)


        for node in execution_nodes:
            node.boot_queues(execution_queue_index[node.name])

        # Fill the list of workers

    async def run(self, bootstraping_data: List[Any]) -> Metrics:
        """
        Main function
        """
        initial_queue = self.__initial_queue()

        # Putting bootstrapping data
        for record in bootstraping_data:
            initial_queue.put_nowait(record)

        # Creating workers
        for worker in self.__workers:
            await worker.start()

        # Waiting for workers to finish
        for queue in self.__ordered_queues():
            await queue.join()

        # Stopping all workers
        for worker in self.__workers:
            await worker.stop()

        return self.__metrics
