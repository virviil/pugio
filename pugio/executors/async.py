import asyncio


class AsyncNode(Node):
    """
    AsyncNode is a base class for all asynchronous nodes.
    """
    def __init__(self, *args, **kwargs):
        super(AsyncNode, self).__init__(*args, **kwargs)
        self.async = True

    def run(self):
        """
        Override this method to implement the logic of the node.
        """
        pass

    def run_async(self):
        """
        Run the node asynchronously.
        """
        self.run()


    async def working_loop(self, in_queue: asyncio.Queue, out_queues: dict(type, asyncio.Queue)):
        pass