import asyncio
from pugio.executor import Executor
from typing import Any, Coroutine, Dict, List, Set, Type
from .link import Link


class Node:
    def __init__(
        self,
        name: str,
        worker_cls: Type[Executor],
        args=[],
        kwargs={},
        pool: int = 1,
        into: List[Link] = [],
    ) -> None:
        self.__name = name
        self.__worker_cls = worker_cls
        self.__args = args
        self.__kwargs = kwargs
        self.__pool = pool
        self.__links = into

    def boot_queues(
        self, input_queue: asyncio.Queue, output_queues: Dict[type, asyncio.Queue]
    ):
        self.__input_queue = input_queue
        self.__output_queues = output_queues

    def run(self) -> List[Coroutine[Any, Any, None]]:
        """
        This function runs all the workers and links.
        """
        return [
            self.__worker_cls(*self.__args, **self.__kwargs).create_task_coroutine(
                self.__input_queue, self.__output_queues
            )
            for i in range(self.__pool)
        ]

    @property
    def name(self):
        return self.__name

    def create_workers(self):
        """
        This function generates a list of workers and DO NOT run them. These objects will be joined on top, and run at the same time.
        """
        [
            self.__worker_cls(*self.__args, **self.__kwargs).create_task_coroutine()
            for i in range(self.__pool)
        ]

    def ensure_links_valid(self, all_nodes_names: Set[str]) -> bool:
        """
        Function checks that all links for this node are valid.
        """
        for link in self.__links:
            if link.is_valid_by_name(all_nodes_names):
                continue
            else:
                return False

        return True

    @property
    def links_names(self) -> List[str]:
        return [link.linked_node_name for link in self.__links]

    @property
    def links(self) -> List[Link]:
        return self.__links
