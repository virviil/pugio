from typing import Set

class LinkError(Exception):
    pass

class Link:
    def __init__(self, linked_node_name: str, link_consume_type: type):
        self.__linked_node_name = linked_node_name
        self.__link_consume_type = link_consume_type

    @property
    def linked_node_name(self) -> str:
        return self.__linked_node_name

    def is_valid_by_name(self, all_nodes_names: Set[str]) -> bool:
        if self.__linked_node_name in all_nodes_names:
            return True
        else:
            raise LinkError(f"Linked node '{self.__linked_node_name}' not found")
